"use strict";
function createList(arey, position = document.body) {
  let list = document.createElement("ul");
  position.append(list);
  let fragment = new DocumentFragment();
  for (const iterator of arey) {
    let li = document.createElement("li");
    li.textContent = iterator;
    fragment.append(li);
  }
  list.append(fragment);
}

createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
function createList(arey, position = document.body) {
  let list = document.createElement("ul");
  position.append(list);
  let newArr = arey.map(function (currentValue) {
    if (Array.isArray(currentValue)) {
      let li = document.createElement("li");
      li.textContent = currentValue;
      list.append(li);
      createList(currentValue, li);
    }
    let li = document.createElement("li");
    li.textContent = currentValue;
    list.append(li);
  });
}
createList([
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],

  "Odessa",
  ["Borispol", ["Borispol", "Irpin"], "Irpin"],
  "Lviv",
  "Dnieper",
]);

let time = 3;
let timer = document.createElement("p");
document.body.append(timer);
countdown();
function countdown() {
  timer.innerHTML = `через ${time} секунд контент будет удален`;
  time--;
  if (time < 0) {
    let item = document.querySelectorAll("ul");
    for (const iterator of item) {
      iterator.remove();
    }
    clearTimeout(timer);
    timer.remove();
  } else {
    setTimeout(countdown, 1000);
  }
}
